package com.personal.puzzle;

import java.util.function.IntBinaryOperator;

public class Sudoku {

    //todo: implement check whether puzzle is valid
    private final int[][] question;
    private final int[] horizontal;
    private final int[] vertical;
    private final int[][] box;
    private final int[][] solution;

    //Lambdas to set/unset 'pos'-th bit of n
    // set pos-th bit of n
    IntBinaryOperator visit = (n, pos) -> n | (1 << pos);
    // unset pos-th bit of n
    IntBinaryOperator noVisit = (n, pos) -> n & (~(1 << pos));

    public Sudoku(int[][] sudoku) {
        this.question = sudoku;
        solution = new int[9][9];
        //stores horizontal numbers data. Each index corresponds to a row. integer value converted to binary string
        // specifies what all numbers are present in the row
        // e.g: if 5th bit is set and 6th is unset, 5 is present in row and 6 is not
        horizontal = new int[9];
        //stores vertical numbers data for each column
        vertical = new int[9];
        //stores numbers present in each 3x3 box
        box = new int[3][3];
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                    setValue(sudoku[i][j], i, j);
            }
        }
    }

    // updates horiz,vertical and box data as and when a value is added/removed
    void setVisitStatus(int i, int j, IntBinaryOperator setStatus) {
        horizontal[i] = setStatus.applyAsInt(horizontal[i], solution[i][j]);
        vertical[j] = setStatus.applyAsInt(vertical[j], solution[i][j]);
        int x = i / 3;
        int y = j / 3;
        box[x][y] = setStatus.applyAsInt(box[x][y], solution[i][j]);
    }

    // checks whether n can be placed in i,j legally
    boolean isVisited(int n, int i, int j) {
        int x = i / 3;
        int y = j / 3;
        return ((horizontal[i] >> n & 1) == 1) || ((vertical[j] >> n & 1) == 1) || ((box[x][y] >> n & 1) == 1);
    }

    //sets n in i,j location and updates horiz,vert and box data accordingly
    void setValue(int n, int i, int j) {
        solution[i][j] = n;
        setVisitStatus(i, j, visit);
    }

    //unsets n in i,j location and updates horiz,vert and box data accordingly
    void unsetValue(int i, int j) {
        setVisitStatus(i, j, noVisit);
        solution[i][j] = 0;
    }

    boolean solve() {
        return backtrack(0);
    }

    boolean backtrack(int curNo) {
        //if all columns are full without rule violations, It is solution!
        if (curNo == 81) {
            return true;
        }

        int x = curNo / 9;
        int y = curNo % 9;

        //if value is already set in sudoku (which is given in question) skip it
        if (solution[x][y] != 0) {
            return backtrack(curNo + 1);
        }


        for (int i = 1; i <= 9; ++i) {
            // from 0-9, backtrack with only valid values - isVisited=true means values is invalid for the cell
            if (!isVisited(i, x, y)) {
                //update flags and solution before recursive check
                setValue(i, x, y);
                //if this call leads to solution, we don't want to undo it
                if (backtrack(curNo + 1)) return true;
                //if above path resulted in a deadend/illegal move, undo and move on
                unsetValue(x, y);
            }
        }

        // if all possible combinations are exhausted, puzzle is wrong.
        return false;
    }


    void prettyPrintSolution() {
        prettyPrint(solution);
    }


    void prettyPrintQuestion() {
        prettyPrint(question);
    }

    void prettyPrint(int[][] sudoku) {
        System.out.println(" _______________________");
        for (int i = 0; i < 9; ++i) {
            System.out.print("| ");
            for (int j = 0; j < 9; ++j) {
                String delim = (j + 1) % 3 == 0 ? " | " : " ";
                System.out.print(sudoku[i][j] + delim);
            }
            System.out.println();
            if ((i + 1) % 3 == 0) {
                System.out.println("|_______________________|");
            }
        }
    }

    public static void main(String[] args) {
        Sudoku sudoku = new Sudoku(
                new int[][]{
                        {0, 0, 0, 0, 0, 5, 2, 0, 0},
                        {0, 8, 9, 0, 2, 0, 0, 3, 0},
                        {0, 6, 0, 0, 0, 0, 0, 0, 0},
                        {0, 2, 3, 0, 5, 0, 0, 8, 0},
                        {0, 0, 0, 4, 0, 0, 0, 0, 7},
                        {1, 0, 0, 0, 0, 0, 0, 0, 0},
                        {6, 0, 0, 0, 9, 0, 0, 0, 0},
                        {5, 0, 0, 0, 0, 0, 0, 1, 0},
                        {0, 9, 1, 0, 0, 7, 0, 0, 8}
                }
        );

        if (sudoku.solve()) {
            sudoku.prettyPrintSolution();
        } else {
            System.out.println("Invalid sudoku");
            sudoku.prettyPrintQuestion();
        }

    }

}