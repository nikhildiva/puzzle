package com.personal.puzzle;

import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public class FizzBuzz {


    void fizzBuzzNaive1(int max) {
        for (int i = 1; i <= max; ++i) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FIZZBUZZ");
            } else if (i % 3 == 0) {
                System.out.println("FIZZ");
            } else if (i % 5 == 0) {
                System.out.println("BUZZ");
            } else {
                System.out.println(i);
            }
        }
    }

    void fizzBuzzNaive2(int max) {
        for (int i = 1; i <= max; ++i) {
            StringBuilder sb = new StringBuilder();

            if (i % 3 == 0) {
                sb.append("FIZZ");
            }
            if (i % 5 == 0) {
                sb.append("BUZZ");
            }

            if (sb.length() == 0) {
                sb.append(i);
            }

            System.out.println(sb);
        }
    }


    static TreeMap<Integer, String> fb = new TreeMap<>();

    static {
        fb.put(3, "FIZZ");
        fb.put(5, "BUZZ");
    }

    void fizzBuzzOptimized(int max) {
        for (int i = 1; i <= max; ++i) {
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<Integer, String> en : fb.entrySet()) {
                if (i % en.getKey() == 0) {
                    sb.append(en.getValue());
                }
            }

            if (sb.length() == 0) {
                sb.append(i);
            }
            System.out.println(sb);
        }
    }

    void fizzBuzzLegendary(int max) {

        for (int i = 1; i <= max; ++i) {
            int currentNo = i;
            Optional<String> fbOutput = fb.entrySet()
                    .stream()
                    .filter(en -> currentNo % en.getKey() == 0)
                    .map(Map.Entry::getValue)
                    .reduce((s1, s2) -> s1 + s2);

            System.out.println(fbOutput.isPresent() ? fbOutput.get() : currentNo);
        }
    }

    public static void main(String[] args) {
        new FizzBuzz().fizzBuzzLegendary(50);
    }

}