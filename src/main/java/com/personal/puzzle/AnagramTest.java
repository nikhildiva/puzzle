package com.personal.puzzle;


import java.util.HashSet;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;


public class AnagramTest {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
            left = null;
            right = null;
        }
    }


    public TreeNode treeMaker(int[] arr) {
        Queue<TreeNode> q = new LinkedList<>();
        q.add(new TreeNode(arr[0]));
        TreeNode head = q.peek();

        int count = 1;
        while (count < arr.length) {
            TreeNode temp = Objects.requireNonNull(q.poll());
            int l = arr[count++];
            int r = arr[count++];

            if (l != -1) {
                temp.left = new TreeNode(l);
                q.add(temp.left);
            }

            if (r != -1) {
                temp.right = new TreeNode(r);
                q.add(temp.right);
            }

        }
        return head;
    }

    HashSet<Integer> hs = new HashSet<>();

    public int solve(int[] A, int[] B) {
        int n = A.length;
        boolean[] vis = new boolean[n + 1];
        return (int) (travel(A, B, new long[n + 1], vis, 1) % 1000000007);
    }

    long travel(int[] A, int[] B, long mem[], boolean[] vis, int i) {
        int n = A.length;

        if (i > n) {
            return 0;
        }

        if (mem[i] == 0) {
            long sum = 0;

            int pr = A[i - 1];
            int gp = A[Math.max(0, pr - 1)];

            if (!(vis[pr] || vis[gp])) {
                vis[i] = true;
                sum = B[i - 1] + travel(A, B, mem, vis, i + 1);
                vis[i] = false;
            }

            sum = Math.max(sum, travel(A, B, mem, vis, i + 1));

            mem[i] = sum;
        }

        return mem[i];
    }


    public static void main(String[] args) {

        AnagramTest a = new AnagramTest();

        System.out.println(a.solve(new int[]{0, 1, 1, 1, 3, 3, 6, 6}, new int[]{90, 2, 100, 4, 5, 6, 7, 8}));
    }
}